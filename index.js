console.log("Mabuhay");
//alert("Hello!!!!");
console. log ( " hello ") ;
console.
log
(
"hello11"
	)
;
// single line comment   

/* multi line comments*/

// [section] - variables
// used to contain data
// usually sttored in a computer's memory

//declaration of variaable

let myVariable;

console.log(myVariable)

let mySecondVariable = 2;

console.log(mySecondVariable)

let productName = "desktop computer";

console.log(productName)

//reassigning value

let friend;

friend = "kate";
friend = "jane";
friend = "cello";

console.log(friend);

//syntax const variableName variaableVaalue

const pet = "bruno";

// pet = "lala";

console.log(pet);

// local and gglobaal variables

let outerVariable  = "hello"; //global variable

{
	let innerVariable = "hello world"; //local variable
    console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "global variable";

{
	const innerExample ="local variable";
	console.log(innerExample);
}

console.log(outerExample);

//multiple declaration

let productCode = "DC017", productBrand = "Dell" ;

console.log(productCode,productBrand)

//strings

let country = 'Philippines';
let province = "Batangas";

let fullAddress = province + "," + country;
console.log(fullAddress);


// \n same as <br>
let mailAddress = "Batangas\n\nPhilippines";
console.log(mailAddress);

let message = "john's employees went home early";
message = 'john\'s employees went home early';
console.log(message);

//numbers
let headcount = 26;
console.log(headcount);

//decimals and fraction
let grade = 98.3;
console.log(grade);

//exponential

let planetDistance = 2e10;
console.log(planetDistance);

// combining strings and numbers
console.log("my grade last sem is " + grade);

//bollean
let isMaarried = false;
let inGoodConduct = true;
console.log("isMaarried: " + isMaarried);
console.log("inGoodConduct: " + inGoodConduct);

//arrays
let grades = [88, 90, 87.6, 85.55];
console.log(grades);

//objects
let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMaarried: false,
	contacts:["098090870", "090923424"],
	addresss: {
		houseNumber: 345,
		city: "manila"
	}
}

console.log(person);







